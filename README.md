# Reto UP - Generador de Reportes

Descripción

Se requiere crear una aplicación que lea un conjunto de archivos “.sql” dada una
ruta de carpeta como entrada. Cada ​ reporte (sentencia SQL) presente en estos archivos
debe contener un conjunto de ​ placeholders que serán parámetros dinámicos para el archivo
“.sql”. Al cargar (leer) el archivo “.sql”, cada vez que la aplicación detecte un placeholder,
debe almacenarlo en la base de datos asociada al archivo leído. Igualmente, se tiene que
guardar en la aplicación el reporte (sentencia SQL) del archivo leído junto con sus
placeholders, así como un nombre para identificarlos en el listado de archivos guardados
posteriormente.

El usuario de la aplicación podrá seleccionar el reporte guardado desde una lista de
reportes. Una vez seleccionado, la aplicación debe pedir al usuario los valores de los
placeholders del reporte y, así, con los valores ya indicados, debe ejecutar el reporte. El
resultado del reporte se debe guardar en un archivo “.csv” para su posterior visualización.

Validaciones requeridas
 
● Es necesario validar que los archivos “.sql” no se carguen más de una vez en el
sistema.
● Es necesario validar que los archivos “.sql” no contengan sentencias que modifiquen
la base de datos; solo se debe permitir sentencias de consultas.
● Es necesario validar que los valores introducidos por el usuario para los placeholders
no contengan sentencias que puedan modificar las base de datos, tales como updates, deletes ​o inserts.

Consideraciones

La aplicación debe ofrecer un menú con las siguientes opciones:

1. Ver y ejecutar reportes
2. Guadar nuevos reportes
3. Configurar la aplicación

El programa debe tener una opción de configuración donde se defina la carpeta (de
entrada) que contiene los archivos “.sql” y la carpeta (de salida) en la que se guardarán los
resultados de los reportes como archivos “.csv”. Igualmente, debe tener la opción de
configuración para definir los delimitadores que usarán los archivos “.sql” para identificar
los placeholders.

Lo siguiente es una lista de reportes que deben estar precargados al momento de
entregar el proyecto:

1. Listar cantidad de productos por estantes
2. Empleado con más facturas generadas
3. Productos más vendido
4. Lista de facturas de alto costo

Se pide que diseñe y estructure su aplicación basándose en Programación Orientada
a Objetos (POO). El grupo de mentores evaluará:

1. Estilo (convención) de codificación
2. Diseño de la solución basada en POO
3. Funcionalidad (que haga lo que se pide)

Recomendaciones

Para completar el reto, se recomienda considerar los siguientes puntos:

● Implementar un archivo de conexión a la bd remota
● Implementar un archivo de configuración para las credenciales de la base de datos remota
● Integrar la gema ‘pg’ en el Gemfile
● Investigar sobre manejo de archivos en Ruby (Guía Resumen del Propedéutico)
● Investigar sobre creación de archivos “.csv”
● Durante la realización del proyecto, surgirán dudas. No es recomendable quedarse
con ellas. Plantéelas a fin de aclararlas, preferiblemente por el chat ​ Promo9 de
Discord.
