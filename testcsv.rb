require_relative 'db/challenge_db'
require_relative 'lib/reports/product'
require 'active_record'
require 'csv'

# Codigo Bueno
def export_to_csv (sql)
  a = ChallengeDB.run_sql(sql)
  attributes = %w[id name code created_at updated_at]
  CSV.open('csv/test.csv','w') do |csv|
    csv << attributes
    a.each do |item|
      csv << item.values_at(*attributes)
    end
  end
end

def printall (sql)
  a = ChallengeDB.run_sql(sql)
  attributes = %w[id name code created_at updated_at]
  #CSV.open('csv/test.csv','w') do |csv|
    #csv << attributes
    a.each do |k, item|
      puts item
    end
  #end
end

sql = ('SELECT * FROM products')

export_to_csv(sql)
#printall(sql)
