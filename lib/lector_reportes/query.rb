class Query < ActiveRecord::Base
  validates :query, :name, :file_path, presence: true 
  validate :only_read

   
  private

  def only_read
    bb = query.split(" ")
    bb.each do |element| 
      if %W(update delete drop UPDATE DELETE DROP).include?(element)
        errors.add(:query,"can't have updates deletes drops" )
      end
    end
  end
end 