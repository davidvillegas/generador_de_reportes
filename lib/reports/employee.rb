
class Employee < ActiveRecord::Base
#{"id"=>1, "name"=>"Helenelizabeth", "last_name"=>"Mainds", "dni"=>"94-217-9111", "supermarket_id"=>1, "created_at"=>"2017-08-11 20:00:00", "updated_at"=>"2017-08-11 20:00:00"}
  def self.to_csv

    sql = ('SELECT * FROM employees')
    @employee = ChallengeDB.run_sql(sql)
    attributes = %w{id name last_name dni supermarket_id created_at updated_at}
    CSV.open('csv/employees.csv','w') do |csv|
      csv << attributes
      @employee.each do |item|
        csv << item.values_at(*attributes)
      end
      p "Reporte de Empleados Generado Exitosamente!"
    end

  end

end
