
class Product < ActiveRecord::Base
#{"id"=>1, "name"=>"Chinese Foods - Cantonese", "code"=>"671-04-2047", "created_at"=>"2017-08-11 20:00:00", "updated_at"=>"2017-08-11 20:00:00"}

  def self.to_csv

    sql = ('SELECT * FROM products')
    @products = ChallengeDB.run_sql(sql)
    attributes = %w{id name code created_at updated_at}
    CSV.open('csv/productos.csv','w') do |csv|
      csv << attributes
      @products.each do |item|
        csv << item.values_at(*attributes)
      end
      p "Reporte de Productos Generado Exitosamente!"
    end

  end

  def self.handle_exception(var)
    find(var)
  rescue	ActiveRecord::ConnectionNotEstablished => e
    p "Registro No Encontrado"
  end

end
