require_relative 'db/challenge_db'
require_relative 'lib/reports/product'
require_relative 'lib/reports/employee'
require 'active_record'
require 'csv'

class Main

	def start(query)
		a = ChallengeDB.run_sql(query)
		a.each do |i|
			puts i
		end
	end

	def export_to_csv
		Product.to_csv
		Employee.to_csv
	end
end

sql = ('SELECT * FROM employees')

#Main.new.start(sql).values
Main.new.export_to_csv
