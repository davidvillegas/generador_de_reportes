class CreatePlaceholders < ActiveRecord::Migration[5.2]
  def change
    create_table :placeholders do |t|
      t.string :name, null:false
      t.references :queries, foreign_key: true
    end
  end
end


