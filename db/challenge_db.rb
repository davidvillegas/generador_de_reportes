require 'active_record'

class ChallengeDB < ActiveRecord::Base
  CONFIG = YAML.load_file(File.join("db/challenge_db_config.yml"))["development"]
  self.abstract_class = true #Indica que no busque otra base de datos que no sea la indicada se utiliza self si estas asignando un valor
  establish_connection(CONFIG)

  def self.run_sql(query) # está definiendo un método de clase que recibe un parámetro
    connection.execute(query) #connection = metodo de clase de ActiveRecord::Base, metodo de clase pq esta definido en un método de clase
  end
end

#execute es un metodo de clase que recibe lo que le pasa a través de connection
# El self.run_sql devuelve un objeto y debemos ubicar la manera de consumirlos
# Con el resultado se puede convertir el archivo .csv
# LEER la documentación de PG
# Matriz de 2 dimensiones

